These cross sections were computed using Starburst99 spectra, with a Kroupa IMF and the 
Geneva tracks with v=0.4 rotation and Z=0.014. They are computed at a stellar age in
the middle of the age bins (in log space) that we use with CHIMES, as follows: 

Bin Number  -  Bin range  -  Bin middle 

1 -       log(age/Myr) < 0.0 - log(age/Myr) = -0.1 
2 - 0.0 < log(age/Myr) < 0.2 - log(age/Myr) = 0.1 
3 - 0.2 < log(age/Myr) < 0.4 - log(age/Myr) = 0.3 
4 - 0.4 < log(age/Myr) < 0.6 - log(age/Myr) = 0.5 
5 - 0.6 < log(age/Myr) < 0.8 - log(age/Myr) = 0.7 
6 - 0.8 < log(age/Myr) < 1.0 - log(age/Myr) = 0.9 
7 - 1.0 < log(age/Myr) < 2.0 - log(age/Myr) = 1.5 
8 - 2.0 < log(age/Myr)       - log(age/Myr) = 2.5 
